# git-workflow

My way to work with git on an everiday base

When you want to clone projects from GitHub you must enter the command in Git bash from here (folder you want to clone):

Copy url adress from Clone or download in repository you want to clone

```
$ git clone https://github.com/ZoranNik91/add-user-js.git
```

# First things first

1. Create a new repository on GitHub. To avoid errors, do not initialize the new repository with README, license, or gitignore files. You can add these files after your project has been pushed to GitHub.

2. Open VS Code than git bash ( in this case VS code terminal ).

3. Change the current working directory to your local project.

4. Initialize the local directory as a Git repository.

```
$ git init
```
5. Add the files in your new local repository. This stages them for the first commit.

```
$ git add .

# Adds the files in the local repository and stages them for commit. To unstage a file, use 'git reset HEAD YOUR-FILE'
```
6. Commit the files that you've staged in your local repository.

```
$ git commit -m "First commit"

# Commits the tracked changes and prepares them to be pushed to a remote repository. To remove this commit and modify the file, use 'git reset --soft HEAD~1' and commit and add the file again.
```
7. At the top of your GitHub repository's Quick Setup page, click (clone or download button) to copy the remote repository URL.

8. In the Command prompt, add the URL for the remote repository where your local repository will be pushed.

```
$ git remote add origin remote repository URL

# Sets the new remote

$ git remote -v

# Verifies the new remote URL
```
9. Push the changes in your local repository to GitHub.

```
$ git push origin master

# Pushes the changes in your local repository up to the remote repository you specified as the origin
```

## CONFIG

Open in VSCode `code ~/.gitconfig` and paste:

```
[core]
	editor = \"C:\\Users\\examp\\AppData\\Local\\Programs\\Microsoft VS Code\\Code.exe\" --wait
	autocrlf = false
	eol = lf
[alias]
	ali = config --get-regexp alias
    pl = pull --rebase
    ph = push --force-with-lease
    ch = checkout
    co = commit # Simple commit
    cm = !git add -A && git commit -m # Commit with message (Suffix with "Some message") 
    st = status # Get current branch local status
    br = branch # List local branches
    lg = log --graph --pretty=format:"%C(yellow)%h%Creset%C(cyan)%d%Creset\\ %C(cyan)(%cr)%Creset\\ %C(green)%cn%Creset\\ %s" # Pretty tree
    rank = shortlog -sn --no-merges # who works harder :)
    diffc = diff --cached HEAD^ # What changed since last commit
[user]
	email = zoran.nikolic1991@gmail.com
	name = Zoran
[branch]
	autosetuprebase = always
[pull]
	rebase = true
[diff]
	tool = default-difftool
[difftool "default-difftool"]
	cmd = code --wait --diff $LOCAL $REMOTE

```

## BRANCHING

To create a new branch for new features or `dev` branch use the 

```
git checkout -b dev
```

or if it's an useful feature

```
git checkout -b feature-admin-panel
```

To switch between branches use

```
git checkout branchname
```

## WORKFLOW

Say we're on `master` branch, we want to create a new branch, work on it, and at the end merge (rebase) that cool branch into master for production.


`git checkout -b dev` Creates a branch called `dev` checkout into it.
At this point we have the exact copy of the `master` branch.  
Let's work! Create files, code code code.
As soon you have created a cool thing, add it to the staging area and commit it with a message

```
git status      # To see what files are changed added modified 
git add index.html
git add css/style.css
git commit -m "Create main layout"
```

let's see if we have more files to add to another commit

```
git status      # yep, there's more files!
git add config.php
git commit -m "Fix username for database"
```

Now we have two commits ready to be pushed to the remote git repo (online).

```
git pull --rebase              # Or use the alias "git pl"
git push --force-with-lease    # Or use the alias "git ph"

git pull origin master -r
```

(If `dev` does not existing on remote, just follow that the console suggests afer doing the `git pl` in the step above.)

Now that you pushed the branch to the remote git repo any other developer can pull and checkout to your new branch and work on it!

TIP: sometimes before starting to work it's OK to pull the branch from remote to get newest colleagues changes before startingto work on it.  
The next day:

`git checkout dev` (if you're on another branch)  
`git pl` to pull the newest code  
work work work! And when you're done add, commit, pull, push!


## REBASE
Urgent changes on `master` branch 

`git checkout master` (if we're in another branch)  
`git pl`  to update our master (pc)  
work work work  
`git add .` to add all modified or new files  
`git co -m "quick fix"` message for the fix
`git pl`  
`git ph`  

  This new quick fixes on the master branch can be now used on production server.  

Now we want to bring changes to master into our `dev` branch  
`git ch dev`  
`git pl`  
`git rebase master`  
`git ph`  After rebasing never pull.  
We want to push to repo, not to pull old code


Now after long time working on the `dev` branch and after testing it we are ready to finnaly bring them in to master  

```
git ch master
git pl
git ch dev
git pl
git rebase master
git ch master
git rebase dev
git ph
```  
## Pull specific commit

If you want to restore specific version of your code you need to copy the commit SHA of that specific code version:


```
git status
git fetch 7767066ead560d711af8cf104029229910b771a8
git checkout FETCH_HEAD
```

more details: https://www.youtube.com/watch?v=FdZecVxzJbk

It's done
The project(`master` branch) is ready to be published tp production server




